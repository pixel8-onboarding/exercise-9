<?php
  /**
   * Tells the browser to allow code from any origin to access
   */
  header("Access-Control-Allow-Origin: *");


  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
  header("Access-Control-Allow-Credentials: true");


  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */
  header("Access-Control-Allow-Headers: Content-Type");

  

require_once('MysqliDb.php');


class API {
    public function __construct()
    {
        $this->db = new MysqliDB('localhost', 'root', '', 'employee');

    }


       /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        if (!is_array($payload)) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Invalid payload format'
            ));
            return;
        }

        // Assuming we're fetching data from a table named 'users'
        $results = $this->db->get('tbl_to_do_List');

        if ($results) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $results
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Failed Fetch Request'
            ));
        }
    }

    /**
       * HTTP POST Request
       *
       * @param $payload
       */
      public function httpPost($payload)
      {
        if(!is_array($payload) || empty($payload)){  
            echo json_encode(array(
            'method' => 'POST',
            'status' => 'failed',
            'message' => 'Invalid payload'
        ));
        return;
            
        }

        $insert_id = $this->db->insert('tbl_to_do_List', $payload);

        if ($insert_id) {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'success',
                'data' => array('id' => $insert_id)
            ));
        } else {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Failed to Insert Data'
            ));
        }
      }


         /**
       * HTTP PUT Request
       *
       * @param $id
       * @param $payload
       */
      public function httpPut($id, $payload)
      {
        if(is_null($id) || empty($payload)){
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Invalid ID'
            ));
            return;

        }


        if(empty($payload)){
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Empty payload'
            ));
            return;
        }
        
        if (isset($payload['id']) && $payload['id'] != $id) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'ID mismatch between URL and payload'
            ));
            return;
        }

       // Use the where() function to specify the ID
    $this->db->where('id', $id);

    // Execute the update query
    $success = $this->db->update('tbl_to_do_List', $payload);

    if ($success) {
        // Fetch the updated record to return in the response
        $this->db->where('id', $id);
        $updatedData = $this->db->getOne('tbl_to_do_List'); //getone?
        echo json_encode(array(
            'method' => 'PUT',
            'status' => 'success',
            'data' => $updatedData
        ));
    } else {
        echo json_encode(array(
            'method' => 'PUT',
            'status' => 'failed',
            'message' => 'Failed to Update Data'
        ));
    }

    } 

    /**
 * HTTP DELETE Request
 *
 * @param $id
 * @param $payload
 */
public function httpDelete($id, $payload)
{
    // Check if id is not null or empty
    if (empty($id)) {
        echo json_encode(array(
            'method' => 'DELETE',
            'status' => 'failed',
            'message' => 'ID is required'
        ));
        return;
    }

    // Check if payload is not empty
    if (empty($payload)) {
        echo json_encode(array(
            'method' => 'DELETE',
            'status' => 'failed',
            'message' => 'Payload is required'
        ));
        return;
    }

    // Check if the id passed in matches the id in the payload
    if (isset($payload['id']) && $payload['id'] != $id) {
        echo json_encode(array(
            'method' => 'DELETE',
            'status' => 'failed',
            'message' => 'ID mismatch between URL and payload'
        ));
        return;
    }

    // Check if payload is an array
    if (is_array($payload)) {
        // Use SQL IN operator
        $this->db->where('id', $payload, 'IN');
    } else {
        // Use single ID
        $this->db->where('id', $id);
    }

    // Execute the delete query
    $success = $this->db->delete('tbl_to_do_List');

    if ($success) {
        echo json_encode(array(
            'method' => 'DELETE',
            'status' => 'success',
            'data' => is_array($payload) ? $payload : array($id)
        ));
    } else {
        echo json_encode(array(
            'method' => 'DELETE',
            'status' => 'failed',
            'message' => 'Failed to Delete Data'
        ));
    }
}


}
// Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

// For GET,POST,PUT & DELETE Request
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    // Check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];

        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));

        $last_index = count($exploded_request_uri) - 1;

        $ids = $exploded_request_uri[$last_index];
    }

    // Payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}

// Create a new instance of an API class
$api = new API();

// Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
    default:
        echo json_encode(array(
            'status' => 'failed',
            'message' => 'Unsupported request method'
        ));
        break;
}



    

?>
