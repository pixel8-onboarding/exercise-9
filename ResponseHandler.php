<?php

class ResponseHandler {
    public static function sendResponse($method, $status, $data = null, $message = null) {
        $response = [
            'method' => $method,
            'status' => $status
        ];

        if ($data !== null) {
            $response['data'] = $data;
        }

        if ($message !== null) {
            $response['message'] = $message;
        }

        echo json_encode($response);
        exit;
    }

    public static function sendSuccessResponse($method, $data = null) {
        self::sendResponse($method, 'success', $data);
    }

    public static function sendFailedResponse($method, $message) {
        self::sendResponse($method, 'failed', null, $message);
    }
}