<?php

namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function iShouldInsertData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('API.php', [
            'id' => '59',
            'task_title' => 'Complete Project',
            'task_name' => 'Implement API Testing',
            'time' => '2023-07-08 15:30:00',
            'status' => 'Inprogress'
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseJsonMatchesJsonPath('$.data.id');
    }

    public function iShouldGetData(ApiTester $I)
    {
        $I->sendGet('API.php');

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseJsonMatchesJsonPath('$.data');
    }

    public function iShouldUpdateData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('API.php/55', [
            'task_title' => 'Complete Project Updated',
            'task_name' => 'Implement API Testing Updated',
            'time' => '2023-07-08 16:00:00',
            'status' => 'Done'
        ]);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseJsonMatchesJsonPath('$.data.id');
    }

    public function iShouldDeleteData(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('API.php/7', ['id' => '7']);

        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
        $I->seeResponseJsonMatchesJsonPath('$.data');
    }
}
